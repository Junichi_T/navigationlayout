//
//  ArticleWebViewController.h
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/08.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic) NSURL *articleURL;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIButton *backButton;

- (id) initWithURL: (NSString *)url;

@end
