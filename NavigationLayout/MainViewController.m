//
//  ViewController.m
//  CustomTableViewTest
//
//  Created by JT on 2015/07/06.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import "MainViewController.h"
#import "ArticleWebViewController.h"
#import "CardViewCell.h"


@interface MainViewController () {
  CGRect viewFrame;
  NSInteger numberOfCellRow;
  
}

@end


@implementation MainViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  viewFrame = self.view.bounds;
  [self loadHeader];
  [self loadTable];
  self.view.backgroundColor = [UIColor whiteColor];
  
  numberOfCellRow = 20;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:YES];
  [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)loadView {
  [super loadView];

}

- (void)loadHeader {
  CGRect headerFrame = viewFrame;
  headerFrame.size.width = self.view.frame.size.width;
  headerFrame.size.height = 200;
  self.headerView = [[HeaderView alloc] initWithFrame:CGRectIntegral(headerFrame)];
  [self.view addSubview:self.headerView];
  NSLog(@"header loaded");
}

- (void)loadTable {
  CGRect tableFrame = CGRectMake(viewFrame.origin.x, viewFrame.origin.y + 150,
                                 viewFrame.size.width, viewFrame.size.height-150);

  self.customTableView = [[UITableView alloc] initWithFrame:CGRectIntegral(tableFrame)];
  self.customTableView.backgroundColor = [UIColor clearColor];
  
  //set delegates
  self.customTableView.delegate = self;
  self.customTableView.dataSource = self;
  
  //set custom cell
  UINib *nib = [UINib nibWithNibName:@"CardViewCell" bundle:nil];
  [self.customTableView registerNib:nib forCellReuseIdentifier:@"Cell"];
  
  self.customTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  [self.view addSubview:self.customTableView];
  
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"cell loaded");
  dispatch_async(dispatch_get_main_queue(), ^{
    [self loadData:indexPath];
    
  });
  
}

- (void)loadData: (NSIndexPath *)indexPath {
  _tableCell.titleLabel.text = [[_articles objectAtIndex:indexPath.row] title];
  
  NSLog(@"%@", [[_articles objectAtIndex:indexPath.row] title]);
  
  [_tableCell setNeedsLayout];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return numberOfCellRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 220;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  _tableCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
  
  _tableCell.backgroundColor = [UIColor clearColor];
  [_tableCell.contentView setUserInteractionEnabled: NO];

  // add touch events
  [_tableCell.button addTarget:self
                        action:@selector(cellPressed:withEvent:)
              forControlEvents:UIControlEventTouchUpInside];
  
  NSLog(@"%@", NSStringFromCGRect(tableView.frame));
  
  return _tableCell;
}


// when button on cell is tapped
- (void) cellPressed: (id) sender withEvent: (UIEvent *) event
{
  NSIndexPath * indexPath = [self.customTableView indexPathForRowAtPoint: [[[event touchesForView: sender] anyObject] locationInView: self.customTableView]];
  NSLog(@"btn.tag %ld",indexPath.row);
  
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"cell tapped: %ld", (long)indexPath.row);
  NSString *url = @"http://www.google.com";
  ArticleWebViewController *articleWebVC = [[ArticleWebViewController alloc] initWithURL:url];
  [self.navigationController pushViewController:articleWebVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  CGPoint currentPoint = [scrollView contentOffset];
  
  if( currentPoint.y >= 0 && currentPoint.y < 200 ){
    // move headerView
    CGRect movedHeaderViewFrame = self.headerView.frame;
    movedHeaderViewFrame.origin.y = (-1)*currentPoint.y;
    self.headerView.frame = movedHeaderViewFrame;
    
    CGRect tableViewFrame = self.customTableView.frame;
    tableViewFrame.origin.y = 150 - (int)currentPoint.y;
    tableViewFrame.size.height = (int)(self.view.frame.size.height - MAX(self.headerView.frame.origin.y, 0) + 50);
    self.customTableView.frame = tableViewFrame;
    NSLog(@"%f, %f", tableViewFrame.origin.y, tableViewFrame.size.height);
  }
}

@end
