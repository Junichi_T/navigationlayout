//
//  ArticleWebViewController.m
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/08.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import "ArticleWebViewController.h"

@interface ArticleWebViewController ()

@end

@implementation ArticleWebViewController

- (id) initWithURL: (NSString *)url {
  if(!self){
    self = [self init];
  }
  _articleURL = [NSURL URLWithString:url];
  
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self.navigationController setNavigationBarHidden:NO animated:YES];

  //CGRect webFrame = [[UIScreen mainScreen] bounds];
  self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
  self.webView.delegate = self;
//  self.backButton.enabled = NO;
  
  NSLog(@"%@", NSStringFromCGRect(self.view.bounds));
  [self.view addSubview:self.webView];
  [self drawButton];
  
  [self.webView loadRequest:[NSURLRequest requestWithURL:_articleURL]];
  
}

- (void)drawButton {
  CGRect buttonFrame = CGRectMake(30, self.view.bounds.size.height-150, 60, 60);
  self.backButton = [[UIButton alloc] initWithFrame:buttonFrame];
  self.backButton = [[UIButton alloc] initWithFrame:CGRectIntegral(buttonFrame)];
  self.backButton.layer.cornerRadius = 30;
  self.backButton.backgroundColor = [UIColor colorWithRed:0 green:169.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
  self.backButton.userInteractionEnabled = YES;
  self.backButton.tag = 110;
  [self.backButton setTitle:@"<" forState:UIControlStateNormal];
  [self.backButton.titleLabel setFont:[UIFont fontWithName:@"ヒラギノ角ゴ ProN W3" size:30.0]];
  self.backButton.alpha = 0.4f;
  
  CALayer *caButtonLayer = self.backButton.layer;
  caButtonLayer.frame = self.backButton.frame;
  caButtonLayer.shadowRadius = 4.0f;
  caButtonLayer.shadowOpacity = 0.35f;
  caButtonLayer.shadowOffset = CGSizeMake(0.0f, 4.0f);
  caButtonLayer.shouldRasterize = YES;
  [caButtonLayer setRasterizationScale:[[UIScreen mainScreen] scale]];
  [caButtonLayer setShouldRasterize:YES];

  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                        action:@selector(buttonTapped)];
  [self.backButton addGestureRecognizer:tap];
  
  [self.view addSubview:self.backButton];
  
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
  if (webView.loading == YES) {
    return;
  }
  
  if(self.webView.canGoBack == YES){
    self.backButton.alpha = 1.0f;
    self.backButton.enabled = YES;
    self.backButton.backgroundColor = [UIColor lightGrayColor];
  }else{
    self.backButton.alpha = 0.4f;
    self.backButton.enabled = NO;
  }
  
}

// back to prev page
- (void) buttonTapped {
  NSLog(@"button tapped");
  //[self.navigationController popViewControllerAnimated:YES];
  if( self.webView.canGoBack == YES ) {
    self.backButton.alpha = 1.0f;
    [self.webView goBack];
  }
  
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
