//
//  MainViewController.h
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/08.
//  Copyright (c) 2015 junet. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "HeaderView.h"
#import "CardViewCell.h"

@interface MainViewController : UIViewController <UITableViewDelegate,
                                                  UITableViewDataSource,
                                                  NSXMLParserDelegate>

@property (strong, nonatomic) UITableView *customTableView;
@property (nonatomic) HeaderView *headerView;
@property (nonatomic) NSArray *articles;
@property (nonatomic) CardViewCell *tableCell;

@end
