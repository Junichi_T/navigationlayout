//
//  HeaderView.m
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/12.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import "HeaderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation HeaderView {

}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    
    // make self from nib file
    UINib *nib = [UINib nibWithNibName:@"HeaderView" bundle:nil];
    self = [nib instantiateWithOwner:nil options:nil][0];
    self.backgroundColor = [UIColor colorWithRed:0 green:169.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    CGRect frame = CGRectZero;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    frame.size.height = 200;
    [self setFrame:frame];
    
  }
  return self;
}


@end
