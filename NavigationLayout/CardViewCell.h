//
//  CustomTableViewCell.h
//  CustomTableViewTest
//
//  Created by JT on 2015/07/06.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *materialCell;
@property (strong, nonatomic) IBOutlet UIView *cardView;
@property (strong, nonatomic) UIImageView *cardImage;

@property (nonatomic) CGRect cardViewBounds;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) NSString *linkURL;
@property (nonatomic) UIButton *button;

@end
