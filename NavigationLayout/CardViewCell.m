//
//  CustomTableViewCell.m
//  CustomTableViewTest
//
//  Created by JT on 2015/07/06.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CardViewCell.h"

@implementation CardViewCell {

}

- (void)drawRect:(CGRect)rect {
  //self.backgroundColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1.0f];
  NSLog(@"%f", rect.origin.x);
  self.backgroundColor = [UIColor clearColor];
  [self drawCardView];
  [self drawCardImage];
  [self drawButton];
  [self drawLabels];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  
  self.selectionStyle = UITableViewCellSelectionStyleNone;

}

- (void)drawCardImage {
  _cardViewBounds = _cardView.bounds;
  CGRect imageFrame = _cardViewBounds;
  imageFrame.size.height = _cardViewBounds.size.height/2+40;
  _cardImage = [[UIImageView alloc] initWithFrame:CGRectIntegral(imageFrame)];
  _cardImage.image = [UIImage imageNamed:@"sampleImage"];
  _cardImage.backgroundColor = [UIColor grayColor];
  _cardImage.clipsToBounds = YES;
  _cardImage.contentMode = UIViewContentModeScaleAspectFill;
  _cardImage.userInteractionEnabled = YES;
  _cardImage.tag = 100;
  
  UIBezierPath *maskPath;
  maskPath = [UIBezierPath bezierPathWithRoundedRect:self.cardView.bounds
                                   byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                         cornerRadii:CGSizeMake(3.0, 3.0)];
  
  CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
  maskLayer.frame = self.cardView.bounds;
  maskLayer.path = maskPath.CGPath;
  _cardImage.layer.mask = maskLayer;
  
  [self.cardView addSubview:_cardImage];
}

- (void)drawCardView {
  CGRect screen = [[UIScreen mainScreen] bounds];
  CGRect frame = CGRectMake(15, 25, (int)screen.size.width-30, 200);
  _cardView.layer.cornerRadius = 3;
  _cardView.backgroundColor = [UIColor greenColor];
  _cardImage.userInteractionEnabled = YES;
  _cardImage.tag = 101;
  _cardView.frame = CGRectIntegral(frame);
  
  CALayer *caLayer = _cardView.layer;
  caLayer.frame = _cardView.frame;
  caLayer.shadowRadius = 3.0f;
  caLayer.shadowOpacity = 0.4f;
  caLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
  caLayer.shouldRasterize = YES;
  // retina screen resolution
  [caLayer setRasterizationScale:[[UIScreen mainScreen] scale]];
  [caLayer setShouldRasterize:YES];
  
  [self addSubview:_cardView];
}

- (void)drawButton {
  CGRect buttonSize = CGRectMake(_cardViewBounds.size.width-100, _cardViewBounds.size.height/2+10, 50, 50);
  _button = [[UIButton alloc] initWithFrame:CGRectIntegral(buttonSize)];
  _button.layer.cornerRadius = 25;
  _button.backgroundColor = [UIColor colorWithRed:0 green:169.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
  _button.userInteractionEnabled = YES;
  _button.tag = 102;
  //_button.titleLabel.text = @"+";
  [_button setTitle:@"+" forState:UIControlStateNormal];
  [_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:30.0]];
  
  CALayer *caButtonLayer = _button.layer;
  caButtonLayer.frame = _button.frame;
  caButtonLayer.shadowRadius = 3.0f;
  caButtonLayer.shadowOpacity = 0.2f;
  caButtonLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
  caButtonLayer.shouldRasterize = YES;
  // retina screen resolution
  [caButtonLayer setRasterizationScale:[[UIScreen mainScreen] scale]];
  [caButtonLayer setShouldRasterize:YES];
  
  [self.cardView addSubview:_button];
  
}

- (void)drawLabels {
  CGRect titleLabelSize = CGRectMake( 10, _cardViewBounds.size.height-50, _cardViewBounds.size.width-20, 40);
  _titleLabel = [[UILabel alloc] initWithFrame:CGRectIntegral(titleLabelSize)];
  _titleLabel.backgroundColor = [UIColor clearColor];
  _titleLabel.numberOfLines = 2;
  _titleLabel.userInteractionEnabled = YES;
  _titleLabel.tag = 103;
  [_titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
  
  [_cardView addSubview:_titleLabel];
  [_cardView sendSubviewToBack:_titleLabel];
}

- (void)prepareForReuse {
  [super prepareForReuse];
  
  for (UIView *subView in [self.contentView subviews]) {
    [subView removeFromSuperview];
  }
}

@end
