//
//  HeaderView.h
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/12.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

@property (nonatomic, strong) IBOutlet UIButton *settingButton;

@end
