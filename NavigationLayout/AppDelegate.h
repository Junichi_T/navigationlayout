//
//  AppDelegate.h
//  MaterialMatomeViewer
//
//  Created by JT on 2015/07/08.
//  Copyright (c) 2015 junet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;


@end

